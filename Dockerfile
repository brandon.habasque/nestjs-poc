FROM node:16

WORKDIR /app/src/app

COPY package*.json ./
COPY prisma ./prisma/

RUN npm install

COPY . .

RUN npm run build

CMD [ "node", "dist/src/main.js" ]